const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

app.on('window-all-closed', function() {
  if(process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('ready', function(){
  mainWindow = new BrowserWindow({ width: 1360, height: 800 });

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, '/public/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.openDevTools();

  mainWindow.on('closed', function() {
    mainWindow = null;
  })
})
