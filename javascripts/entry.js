import '../less/main.less';
import React from "react";
import ReactDOM from 'react-dom';

ReactDOM.render(
  <div className="myDiv">Hello Electron made by Atif!</div>,
  document.getElementById('content')
);
